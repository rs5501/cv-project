function project()
im = imread("20220818_lb_p4r_twri_mosaic593.TIF");
im2 = im(:,:,1:3);
figure, imshow(im2);

im2 = rgb2gray(im2);
bw = imbinarize(im2, graythresh(im2));

figure, imshow(bw);


end